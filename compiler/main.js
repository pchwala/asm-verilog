    function pad ( num, size ) {
      return ( Math.pow( 10, size ) + ~~num ).toString().substring( 1 );
    }

    String.prototype.lpad = function(length) {
    var str = this;
    while (str.length < length)
        str = "0" + str;
    return str;
}

$(document).ready(function () {

  var bin_results = [];
  var hex_results = [];
  
  var compile = function (tokens) {
    bin_results = [];
    hex_results = [];

    var o = {
      size: {
        instruction: 4,
        reg1: 2,
        reg2: 2,
        data: 8
      },

      instructions: {
        MOV1:   '0',
        MOV2:   '1',
        ADD:    '100',
        SUB:    '101',
        CMP:    '110',
        JG:     '111',
        JE:     '1000',
        JL:     '1001',
        INT:    '1010',
        NOP:    '1011'
      },

      regs: {
        R1: '0',
        R2: '1',
        R3: '10',
        R4: '11'
      },
    };

    var result = [];

    single_reg_instr = function (inst) {
      r = o.instructions[inst[0]].lpad(o.size.instruction) + " ";
      r += o.regs[inst[1]].lpad(o.size.reg1) + " ";
      r += "0".lpad(o.size.reg2) + " ";
      r += "0".lpad(o.size.data);

      return r;
    };

    double_reg_instr = function (inst) {
      r = o.instructions[inst[0]].lpad(o.size.instruction) + " ";
      r += o.regs[inst[1]].lpad(o.size.reg1) + " ";
      r += o.regs[inst[2]].lpad(o.size.reg2) + " ";
      r += "0".lpad(o.size.data);

      return r;
    };

    _.each(tokens, function(inst) {
      console.log(inst);

      var r = [];

      switch(inst[0]) {
      case 'MOV1':
        r = o.instructions.MOV1.lpad(o.size.instruction) + " ";
        r += o.regs[inst[1]].lpad(o.size.reg1) + " ";
        r += "0".lpad(o.size.reg2) + " ";
        r += inst[2].lpad(o.size.data);
        break;
      case 'MOV2':
        r = double_reg_instr(inst);
        break;
      case 'ADD':
        r = double_reg_instr(inst);
        break;
      case 'CMP':
        r = double_reg_instr(inst);
        break;
      case 'SUB':
        r = double_reg_instr(inst);
        break;
      case 'JG':
        r = single_reg_instr(inst);
        break;
      case 'JE':
        r = single_reg_instr(inst);
        break;
      case 'JL':
        r = single_reg_instr(inst);
        break;
      case 'INT':
        r = o.instructions.INT.lpad(o.size.instruction) + " ";
        r += o.regs[inst[2]].lpad(o.size.reg1) + " ";
        r += "0".lpad(o.size.reg2) + " ";
        r += inst[1].lpad(o.size.data);
        break;
      case 'NOP':
        r = o.instructions.NOP.lpad(o.size.instruction) + " ";
        r += "0".lpad(o.size.reg1) + " ";
        r += "0".lpad(o.size.reg2) + " ";
        r += "0".lpad(o.size.data);
        break;
      default:
        //code to be executed if n is different from case 1 and 2
      }
  
      var bin = r;
      bin_results.push(bin);

      var hex = parseInt(bin.replace(/ /g, ''), 2).toString(16).lpad(4);
      hex_results.push(hex);
    });
  };

  var display_tokens = function (tokens) {
    var $tokens = $("#tokens");
    var out = "";

    _.each(tokens, function (token) {
      out += JSON.stringify(token) + "\n";
    });

    $tokens.val(out);
  };

  var display_bin = function (bins) {
    var $bin = $("#bin");
    var out = "";

    _.each(bins, function (bin) {
      out += bin + "\n";
    });

    $bin.val(out);
  };

  var display_hex = function (hexs) {
    var $hex = $("#hex").html('');
    var n = 8;

    var rows = _.groupBy(hexs, function(element, index){
      return Math.floor(index/n);
    });

    rows = _.toArray(rows);

    _.each(rows, function (row) {
      $tr = $("<tr></tr>");

      _.each(row, function (cell) {
        $td = $("<td>" + cell + "</td>");

        $tr.append($td);
      });

      while($tr.children('td').length < 8) {
        $tr.append( $("<td></td>") );
      };

      $hex.append($tr);
    });
  }

  $("#compile").on('click', function () {

    var input = $("#input").val();
    var output = parser.parse(input);

    compile(output);
    display_tokens(output);
    display_bin(bin_results);
    display_hex(hex_results);

    //$("#bin").val(bin_results);
    //$("#hex").val(hex_results);
  });

});
